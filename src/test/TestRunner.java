import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;


@CucumberOptions(
        glue = {"app.stepdefs"},
        features = {"src/main/java/app/features"}
)
@RunWith(Cucumber.class)
public class TestRunner {
}
