package app.config;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.*;

import static java.util.Objects.requireNonNull;

@Slf4j
@Configuration
@EnableConfigurationProperties(TestProperties.class)
@PropertySource(value = "local.properties", ignoreResourceNotFound = true)
@ComponentScan(basePackages = "app.*")
public class TestAppConfiguration {

    @Value("${webdriver.path}")
    private String chromePath;

    @Autowired
    private TestProperties testProperties;

    @Bean
    public WebDriver createDriver(){
        log.info("Properties: {}", testProperties);
        WebDriver driver = null;
        switch (testProperties.getBrowser()) {
            case "chrome":
                if (System.getProperty("webdriver.chrome.driver") == null) {
                    System.setProperty("webdriver.chrome.driver", requireNonNull(chromePath));
                }
                driver = new ChromeDriver();
                break;
            case "ie":
                driver = new InternetExplorerDriver();
                break;
            default:
                throw new IllegalArgumentException("Unknown browser " + testProperties.getBrowser());
        }
        Runtime.getRuntime().addShutdownHook(
                new Thread(driver::quit)
        );
        return driver;
    }

    @Bean
    public Actions actions(WebDriver driver) {
        return new Actions(driver);
    }
}
