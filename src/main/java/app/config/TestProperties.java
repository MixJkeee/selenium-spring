package app.config;


import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Getter;


@Getter
@Setter
@ToString
@ConfigurationProperties
public class TestProperties {
    private String targetUrl;
    private String browser;
    private Integer explicitWait;

}
