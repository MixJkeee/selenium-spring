package app.stepdefs;

import app.html.pages.LoginPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.java.StepDefAnnotation;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static ru.yandex.qatools.htmlelements.matchers.MatcherDecorators.should;
import static ru.yandex.qatools.htmlelements.matchers.MatcherDecorators.timeoutHasExpired;
import static ru.yandex.qatools.htmlelements.matchers.WebElementMatchers.isDisplayed;

public class CommonStepDefs extends BaseStepDef {

    @Autowired
    private LoginPage loginPage;

    @When("^User open ClauseMatch")
    public void user_open_ClauseMatch() {
        driver.get(testProperties.getTargetUrl());
    }
    @Then("^Login Page > Login form is(| not) displayed$")
    public void login_Page_Login_form_is_displayed(String expected) {
        boolean isDisplayed = expected.isEmpty();
        if (isDisplayed) {
            assertThat(loginPage.getSearch(), should(isDisplayed()).whileWaitingUntil(timeoutHasExpired()));
        } else {
            assertThat(loginPage.getLoginForm(), should(not(isDisplayed())).whileWaitingUntil(timeoutHasExpired()));
        }
    }

    @And("^Login Page > Fill login form with \"(.+)\" and \"(.+)\" credentials$")
    public void fillLoginForm(String login, String password) {
        loginPage.getLoginForm().fillForm(login, password);
    }
}
