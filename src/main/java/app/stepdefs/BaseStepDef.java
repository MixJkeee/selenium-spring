package app.stepdefs;


import app.config.TestAppConfiguration;
import app.config.TestProperties;
import app.html.blocks.AbstractBlock;
import app.html.pages.LoginPage;
import app.util.Wait;
import cucumber.runtime.java.StepDefAnnotation;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = {TestAppConfiguration.class})
public class BaseStepDef {

    @Autowired
    protected TestProperties testProperties;

    @Autowired
    protected WebDriver driver;

    @Autowired
    protected Wait wait;
}
