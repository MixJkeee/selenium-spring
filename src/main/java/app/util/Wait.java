package app.util;

import app.config.TestProperties;
import app.util.enums.WaitCondition;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;

import java.util.function.Function;
import java.util.function.Supplier;

import static app.util.CustomExpectedConditions.elementExists;
import static app.util.CustomExpectedConditions.typifiedElementExists;
import static app.util.WebElementUtils.createElement;
import static org.openqa.selenium.support.ui.ExpectedConditions.refreshed;

@Slf4j
@Component
public class Wait {

    private final WebDriverWait webDriverWait;

    @Autowired
    public Wait(WebDriver driver, TestProperties properties) {
        this.webDriverWait = new WebDriverWait(driver, properties.getExplicitWait());
    }

    public WebElement forElement(WebElement element, WaitCondition... waitConditions) {
        assert waitConditions.length > 0;
        for (WaitCondition waitCondition : waitConditions) {
            log.info(waitCondition.toString());
            element = webDriverWait.ignoring(WebDriverException.class)
                    .until(refreshed(waitCondition.getElementType().apply(element)));
        }
        return element;
    }

    public WebElement forElement(By locator, WaitCondition... waitConditions) {
        assert waitConditions.length > 0;
        WebElement element = null;
        for (WaitCondition waitCondition : waitConditions) {
            element = webDriverWait.ignoring(WebDriverException.class)
                    .until(waitCondition.getLocatorType().apply(locator));
        }
        return element;
    }

    public WebElement forWebElement(Supplier<WebElement> getter, WaitCondition... waitConditions) {
        WebElement element = webDriverWait.until(elementExists(getter));
        return forElement(element, waitConditions);
    }

    public <T extends TypifiedElement> T forElement(By locator, Class<T> elementClazz, WaitCondition... waitConditions) {
        return createElement(forElement(locator, waitConditions), elementClazz);
    }

    @SuppressWarnings("unchecked")
    public <T extends TypifiedElement> T forElement(T element, WaitCondition... waitConditions) {
        WebElement el = forElement(element.getWrappedElement(),
                waitConditions);
        Class<T> tClass = (Class<T>) element.getClass();
        return createElement(el, tClass);
    }

    public <T extends TypifiedElement> T forTypifiedElement(Supplier<T> getter, WaitCondition... waitConditions) {
        T element = webDriverWait.until(typifiedElementExists(getter));
        return forElement(element, waitConditions);
    }

    public <T extends TypifiedElement> T forTypifiedElement(Function<String, T> finder, String parameter, WaitCondition... waitConditions) {
        T element = webDriverWait.until(typifiedElementExists(finder, parameter));
        return forElement(element, waitConditions);
    }


}
