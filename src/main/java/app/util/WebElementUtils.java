package app.util;

import app.html.blocks.AbstractBlock;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Arrays.stream;

public class WebElementUtils {

    private WebElementUtils() {
    }

    public static  <T extends TypifiedElement> T createElement(WebElement element, Class<T> clazz) {
        try {
            Constructor<T> constructor = clazz.getDeclaredConstructor(WebElement.class);
            constructor.setAccessible(true);
            return constructor.newInstance(element);
        } catch (IllegalAccessException | InstantiationException |
                NoSuchMethodException | InvocationTargetException e) {
            throw new IllegalArgumentException("Can't create instance", e);
        }
    }

    public static List<Field> getAllHtmlBlocks(Class clazz) {
        return stream(clazz.getDeclaredFields())
                .filter(field -> AbstractBlock.class.isAssignableFrom(field.getType())
                        && field.getType() != AbstractBlock.class)
                .collect(Collectors.toList());
    }

    public static  <T> Field getFieldByType(Class clazz, Class<T> fieldType) {
        return stream(clazz.getDeclaredFields())
                .filter(field -> field.getType() == fieldType)
                .findFirst()
                .orElseGet(() ->
                        stream(clazz.getSuperclass().getDeclaredFields())
                                .filter(field -> field.getType() == fieldType)
                                .findFirst()
                                .orElseThrow(IllegalArgumentException::new));
    }
}
