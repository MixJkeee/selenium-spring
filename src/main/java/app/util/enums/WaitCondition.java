package app.util.enums;


import app.util.CustomExpectedConditions;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.function.Function;

@Getter
@RequiredArgsConstructor
public enum WaitCondition {
    EXIST(CustomExpectedConditions::elementExists, ExpectedConditions::presenceOfElementLocated),
    VISIBLE(ExpectedConditions::visibilityOf, ExpectedConditions::visibilityOfElementLocated),
    ENABLED(ExpectedConditions::elementToBeClickable, ExpectedConditions::elementToBeClickable);

    private final Function<WebElement, ExpectedCondition<WebElement>> elementType;
    private final Function<By, ExpectedCondition<WebElement>> locatorType;
}
