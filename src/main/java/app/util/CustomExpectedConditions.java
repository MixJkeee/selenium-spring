package app.util;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;

import javax.validation.constraints.NotNull;
import java.util.function.Function;
import java.util.function.Supplier;

public class CustomExpectedConditions {

    private CustomExpectedConditions() {
    }

    public static ExpectedCondition<WebElement> elementExists(@NotNull WebElement element) {
        return driver -> {
            try {
                return element.findElement(By.xpath("self::*"));
            } catch (NoSuchElementException | StaleElementReferenceException e) {
                return null;
            }
        };
    }

    public static ExpectedCondition<WebElement> elementExists(@NotNull Supplier<WebElement> getter) {
        return driver -> {
            try {
                return getter.get();
            } catch (NoSuchElementException | StaleElementReferenceException e) {
                return null;
            }
        };
    }

    public static <T extends TypifiedElement> ExpectedCondition<T> typifiedElementExists(@NotNull Supplier<T> getter) {
        return driver -> {
            try {
                return getter.get();
            } catch (NoSuchElementException | StaleElementReferenceException e) {
                return null;
            }
        };
    }

    public static <T extends TypifiedElement> ExpectedCondition<T> typifiedElementExists(@NotNull Function<String, T> finder, String parameter) {
        return driver -> {
            try {
                return finder.apply(parameter);
            } catch (Exception e) {
                return null;
            }
        };
    }
}
