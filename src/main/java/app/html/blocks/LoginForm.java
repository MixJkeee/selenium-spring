package app.html.blocks;

import lombok.Getter;
import lombok.Setter;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.TextInput;

@Getter
@Setter
@FindBy(xpath = "descendant::form[(ancestor::*[@id = 'center'])]")
public class LoginForm extends AbstractBlock {

    @Name("Login input")
    @FindBy(xpath = "descendant::input[1]")
    private TextInput loginInput;

    @Name("Password input")
    @FindBy(xpath = "descendant::input[2]")
    private TextInput passwordInput;

    public void fillForm(String login, String password) {
        sendKeys(loginInput, login);
        sendKeys(passwordInput, password);
    }


}
