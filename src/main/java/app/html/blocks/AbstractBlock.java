package app.html.blocks;

import app.util.Wait;
import lombok.Getter;
import lombok.Setter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import ru.yandex.qatools.htmlelements.element.TextInput;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;

import static app.util.enums.WaitCondition.*;

@Getter
@Setter
public class AbstractBlock extends HtmlElement {

    protected Wait wait;
    protected Actions actions;

    protected void sendKeys(TextInput input, String text) {
        wait.forElement(input, VISIBLE, ENABLED).sendKeys(text);
    }

    protected void click(WebElement element) {
        wait.forElement(element, VISIBLE, ENABLED).click();
    }

    protected <T extends TypifiedElement> void click(T element) {
        wait.forElement(element, VISIBLE, ENABLED).click();
    }

}
