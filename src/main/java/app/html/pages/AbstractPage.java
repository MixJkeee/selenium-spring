package app.html.pages;

import app.util.Wait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.Field;

import static app.util.WebElementUtils.getAllHtmlBlocks;
import static app.util.WebElementUtils.getFieldByType;
import static ru.yandex.qatools.htmlelements.loader.HtmlElementLoader.populatePageObject;

public class AbstractPage {

    protected WebDriver driver;
    protected Actions actions;
    protected Wait wait;

    @Autowired
    public AbstractPage(WebDriver driver, Actions actions, Wait wait) {
        this.driver = driver;
        this.actions = actions;
        this.wait = wait;
        populatePageObject(this, driver);
        injectDependencies();
    }

    protected void injectDependencies() {
        getAllHtmlBlocks(this.getClass())
                .forEach(field -> {
                    Field actionsField = getFieldByType(field.getType(), Actions.class);
                    Field waitField = getFieldByType(field.getType(), Wait.class);
                    field.setAccessible(true);
                    actionsField.setAccessible(true);
                    waitField.setAccessible(true);
                    try {
                        if (actionsField.get(field.get(this)) == null) {
                            actionsField.set(field.get(this), actions);
                            waitField.set(field.get(this), wait);
                        }
                    } catch (IllegalAccessException e) {
                        throw new RuntimeException(e);
                    }
                });
    }
}
