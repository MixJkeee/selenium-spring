package app.html.pages;

import app.html.blocks.LoginForm;
import app.util.Wait;
import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.springframework.stereotype.Component;
import ru.yandex.qatools.htmlelements.element.Button;

@Getter
@Component
public class LoginPage extends AbstractPage{

    private LoginForm loginForm;

    @FindBy(name = "btnK")
    private Button search;

    public LoginPage(WebDriver driver,
                     Actions actions,
                     Wait wait) {
        super(driver, actions, wait);
    }

}
